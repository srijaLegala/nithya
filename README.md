Q1. -------------------------------------------------------------------------------------------------------
class A {
	public B b = new B();
}

class B{
	int c = 10;
	int a = 30;
}

class C {
	public B b = new B();
}

public class Javachal1 {

	public static void main(String[] args) {

		A a = new A();
		B b = new B();
		C c = new C();
		
		a.b.c = c.b.a;
		
	}

}
-------------------------------------------------------------------------------------------- END ---------------------------------------------------------------------------------------

Q2.  -------------------------------------------------------------------------------------------------------

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Month;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class JavaChal2 {


	public static void main(String[] args) throws IOException 
	{ 
		String maindirpath = "F:\\documents"; 

		File maindir = new File(maindirpath); 

		if(maindir.exists() && maindir.isDirectory()) 
		{ 
			List<String[]> list = new ArrayList<String[]>();
			File arr[] = maindir.listFiles(); 
			for (File file : arr) {
				Path filep = Paths.get(file.getAbsolutePath());

				BasicFileAttributes attr = Files.readAttributes(filep, BasicFileAttributes.class);
				Month month =attr.creationTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().getMonth();

				String[] temp = {file.getName(), month.toString()};
				list.add(temp);
			}

			Map<String, List<String[]>> map = list.stream()
					.collect(Collectors.groupingBy(s -> s[1]));

			Map finalMap = new HashMap<String, Integer>();
			map.forEach((a,b)->finalMap.put(a, b.size()));

			System.out.println(finalMap);

		}  
	} 
}
-------------------------------------------------------------------------------------------- END ---------------------------------------------------------------------------------------